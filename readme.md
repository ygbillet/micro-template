Microtpl
=========

Description
-----------

It's a micro templating system in PHP. 
I needed a simple templating system that replace php variable (markers) in an HTML template. 
I would like to first generate the content view and then include it in a master layout which contains some common view (menu, sidebar, etc.).
The micro templating system uses a two step rendering (EEA pattern from Martin Fowler). 
It :

1. Generate HTML for the domain view. 
2. Include previous generated data in the master layout in order to render a full HTML page. (replace the `$content` marker)

Installation
------------

1. Copy `Template.php` and `View.php` in your project directory. 
2. Include `View.php` in your php files. 

Usage
-----

- `layout` refers to the general template which include common elements (menu, sidebar, footer, header, etc.)
- `content` refers to the domain view.

### Layout : File view/layout.html.php

    </html>
    <head>
        <title><?php echo $title; ?></title>
    </head>
    <body>
        <div id="content">
        <?php echo $content; ?>
        </div>
    </body>
    <html>

### Content : File view/view.html.php ($content in layout.html.php)

    <h1>This is an header level 1</h1>
   
    <p>Hello <?= $name ?> !!</p>

### Create a new View object (index.php)

    $v = new View();

### Add a master layout and set data (index.php)

    $v->set(array(
        'layout' => array(
            'path' => 'view/layout.html.php',
            'data' => array(
                'title' => "This is a title")
            )
        )
    );

or

    $v->layout()->set(array('path' => 'view/layout.html.php'));
    $v->layout()->set(array(
        'data' => array(
            'title' => "This is a title"
            )
        )
    );

### Add a domain view (index.php)

    $v->set(array(
        'layout' => array(
            'path' => 'view/view.html.php',
            'data' => array(
                'name' => "World")
            )
        )
    );

or

    $v->content()->set(array('path' => 'view/view.html.php'));
    $v->content()->set(array(
        'data' => array(
            'name' => "World"
            )
        )
    );

### Change data for a marker (in this example for domain view) (index.php)

    $v->content()->set(array(
        'data' => array(
            'name' => "Bob"
            )
        )
    );

### Render view (content view + layout view) (index.php)

    echo $v->render();

To-do
-----
- Add it to Composer ?
