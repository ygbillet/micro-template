<?php

require_once dirname(__FILE__).'/../microtpl/View.php';

use ygbillet\microtpl\View;

$v = new View();

$v->layout()->set(array('path' => 'view/layout.html.php'));
$v->layout()->set(array(
    'data' => array(
        'title' => "This is a title"
        )
    )
);

$v->set(array(
    'content' => array(
        'path' => 'view/view.html.php',
        'data' => array(
            'name' => 'world'
            )
        )
    )
);

$v->render();

?>