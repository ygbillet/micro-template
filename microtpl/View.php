<?php

namespace ygbillet\microtpl;

require_once dirname(__FILE__).'/Template.php';

/**
 *  This is a simple templating system for PHP using the EEA pattern 
 * `Two Step View`. It use the code from Template.php
 *
 *
 */
class View {

    protected $_layout;     // Layout
    protected $_content;    // Data from domain
    protected $_config;     // Store init config

    public function __construct(array $config = array()) {
        $defaults = array('init' => true);
        $this->_config = $config + $defaults;

        if ($this->_config['init']) {
            $this->_init();
        }
    }

    public function _init() {
        $this->_layout = new Template();
        $this->_content = new Template();

        $this->set($this->_config);
    }

    public function layout() {
        return $this->_layout;
    }

    public function content() {
        return $this->_content;
    }

    /**
     * Render view for application using the `Two Step View` pattern. It first
     * render the `content` which is the domain data. It then render the 
     * `layout`. The layout contains the _reserved marker_ `$content` in order 
     * to insert the rendering of the first step.
     *
     * @param boolean   $twostep Option to not generate the domain data.
     *
     * @return void
     */
    public function render($twostep = true) {

        if ($twostep) {
            $this->_layout->set(array(
                'data' => array(
                    'content' => $this->_content->render()
                    )
                )
            );
        }

        echo $this->_layout->render();
    }

    public function set(array $data = array()) {
        foreach (array('content','layout') as $key) {
            if (isset($data[$key])) {
                $this->{"_{$key}"} = new Template;
                $this->{"_{$key}"}->set($data[$key]);
            }
        }
    }
}