<?php

namespace ygbillet\microtpl;

class Template {

    /**
     * Contains path to the HTML template which embed markers.
     */
    protected $_path;

    /**
     * Contains an associative array of variables to be sent to the template    
     */
    protected $_data;
    protected $_config;     // store init config

    public function __construct(array $config = array()) {
        $defaults = array('init' => true);
        $this->_config = $config + $defaults;

        if ($this->_config['init']) {
            $this->_init();
        }
    }

    public function _init() {
        $this->_data = array();
        $this->set($this->_config);
    }

    /**
     * Renders information into HTML by replacing embedded markers by data from
     * the associative array `$this->_data`. 
     *
     * If template file cannot be found, the function return false.
     */
    public function render() {
        if (isset($this->_path) && file_exists($this->_path)) {
            extract( $this->_data );
            ob_start(); 
            require($this->_path);

            return ob_get_clean();
        }
        return false;
    }

    /**
     * Configure template path's and send data for markers in template.
     * 
     * @param an array of options : 
     *      - `'data'` : An associative array of variable to the template. These
     *          are merged on top of any variables. 
            - `'path'` : The path to the template.

     * @return void
     */
    public function set( array $options=array() ) {
        if (isset($options['data'])) {
            $this->_data = $options['data'] + $this->_data;
        }
        if (isset($options['path'])) {
            $this->_path = $options['path'];
        }
    }
}